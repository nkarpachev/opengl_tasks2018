#include "Mesh.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>


glm::vec3 getTorusVertex(float r0, float r1, float u, float v)
{
    float x = (r0 + r1 * cos(u)) * cos(v);
    float y = (r0 + r1 * cos(u)) * sin(v);
    float z = r0 * sin(u);

    return glm::vec3(x, y, z);
}


glm::vec3 getTorusNormal(float r0, float r1, float u, float v) 
{
    glm::vec3 dr_du = glm::vec3(
        - r1 * cos(v) * sin(u),
        -sin(v) * r1 * sin(u),
        r0 * cos(u) 
        );

    glm::vec3 dr_dv = glm::vec3(
        -(r0 + r1 * cos(u)) * sin(v),
        (r0 + r1 * cos(u)) * cos(v),
        0
        );

    glm::vec3 normal = glm::cross(dr_du, dr_dv);
    glm::normalize(normal);

    return normal;
}


MeshPtr makeTorus(float smallRadius, float bigRadius, unsigned int N) 
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    for (unsigned int i = 0; i < N; i++)
    {
        float u = 2.0f * glm::pi<float>() * i / N;
        float u1 = 2.0f * glm::pi<float>() * (i + 1) / N;

        for (unsigned int j = 0; j < N; j++) 
        {
            float v = 2.0f * glm::pi<float>() * j / N;
            float v1 = 2.0f * glm::pi<float>() * (j + 1) / N;

            vertices.push_back(getTorusVertex(smallRadius, bigRadius, u, v));
            vertices.push_back(getTorusVertex(smallRadius, bigRadius, u1, v));
            vertices.push_back(getTorusVertex(smallRadius, bigRadius, u, v1));

            normals.push_back(getTorusNormal(smallRadius, bigRadius, u, v));
            normals.push_back(getTorusNormal(smallRadius, bigRadius, u1, v));
            normals.push_back(getTorusNormal(smallRadius, bigRadius, u, v1));

            vertices.push_back(getTorusVertex(smallRadius, bigRadius, u1, v));
            vertices.push_back(getTorusVertex(smallRadius, bigRadius, u, v1));
            vertices.push_back(getTorusVertex(smallRadius, bigRadius, u1, v1));

            normals.push_back(getTorusNormal(smallRadius, bigRadius, u1, v));
            normals.push_back(getTorusNormal(smallRadius, bigRadius, u, v1));
            normals.push_back(getTorusNormal(smallRadius, bigRadius, u1, v1));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());


    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}
