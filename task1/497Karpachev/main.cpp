#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"


class SampleApplication : public Application 
{
public:
	MeshPtr _torus;

	int _polygonMode = GL_FILL;
	ShaderProgramPtr _shader;

	unsigned int detalization = 50;
	float r0 = 1.0f;
	float r1 = 1.0f;

	void makeScene() override
	{
		Application::makeScene();

		//_cameraMover = std::make_shared<OrbitCameraMover>();

		//Создаем меш с тором
		_torus = makeTorus(r0, r1, detalization);
		
		//Создаем шейдерную программу        
		_shader = std::make_shared<ShaderProgram>("497KarpachevData/shaderNormal.vert", "497KarpachevData/shader.frag");
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::RadioButton("Fill polygons", &_polygonMode, GL_FILL);
			ImGui::RadioButton("Grid", &_polygonMode, GL_LINE);

			ImGui::Text("%d points", _torus->getVertexCount());
		}

		ImGui::End();
	}

	void handleKey(int key, int scancode, int action, int mods) override {
		Application::handleKey(key, scancode, action, mods);

		if (key == GLFW_KEY_KP_SUBTRACT) 
		{	
			if (detalization > 5) {
				detalization *= 0.9;
				makeScene();
			}
		}

		if (key == GLFW_KEY_KP_ADD) 
		{	
			if (detalization < 200) 
			{ 
				detalization /= 0.8;
				makeScene();
			}
		}

		if (key == GLFW_KEY_PAGE_UP)
		{
			r1 /= 0.9;
			makeScene();
		}

		if (key == GLFW_KEY_PAGE_DOWN)
		{
			r1 *= 0.9;
			makeScene();
		}
	}


	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glPolygonMode(GL_FRONT_AND_BACK, _polygonMode);
		//Устанавливаем шейдер.
		_shader->use();

		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		//Рисуем меш
		_shader->setMat4Uniform("modelMatrix", _torus->modelMatrix());
		_torus->draw();


	}
};


int main(int argc, char** argv)
{
	SampleApplication app;
	app.start();

    return 0;
}
